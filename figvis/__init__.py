# -*- encoding: utf-8 -*-

"""
Figment Visualization
=====================

Figment Visualization is a framework to visualize data using templates.


"""


from importlib.metadata import version as _version

from figvis import figment, transform, data


__author__ = "Umesh Mohan <moh@nume.sh>"
__copyright__ = "2021, Umesh Mohan"

__version__ = _version("figvis")
