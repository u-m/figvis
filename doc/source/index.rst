.. automodule:: figvis


Navigation
**********

.. toctree::
   :maxdepth: 1
   :caption: Submodules

.. toctree::
   :maxdepth: 1
   :caption: CLI
   
.. toctree::
   :maxdepth: 1
   :caption: Architecture

.. toctree::
   :caption: Indices
   
   glossary

Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
* :doc:`glossary`
